# Frontend Project TEL335



## Description
El proyecto va a estar corriendo en el puerto 8081 en nuestro localhost. Para comprobar su funcionamiento, puede dirigirse a su navegador y colocar en el buscador http://localhost:8081/

## Instructions
Luego de haber clonado este repositorio, ejecute el siguiente comando en la terminal, en la ubicación del proyecto, para instalar las dependencias de este proyecto:
```
npm install
```
Después, para ejecutar el proyecto:
```
npm start
```
