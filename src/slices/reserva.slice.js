import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { setMessage } from "./message.slice";
import reservaService from "../services/reserva.service";

export const createReserva = createAsyncThunk("reserva/createReserva", async ({ horarioId, userId }, thunkAPI) => {
    try {
        const response = await reservaService.createReserva(horarioId, userId);
        thunkAPI.dispatch(setMessage(response.data.message));
        return response.data;
    } catch (error) {
        const message =
            (error.response && error.response.data && error.response.data.message) ||
            error.message ||
            error.toString();
        thunkAPI.dispatch(setMessage(message));
        return thunkAPI.rejectWithValue();
    }
});
export const deleteReserva = createAsyncThunk("reserva/deleteReserva", async ({ horarioId, userId }, thunkAPI) => {
    try {
        const response = await reservaService.deleteReserva(horarioId, userId);
        thunkAPI.dispatch(setMessage(response.data.message));
        return response.data;
    } catch (error) {
        const message =
            (error.response && error.response.data && error.response.data.message) ||
            error.message ||
            error.toString();
        thunkAPI.dispatch(setMessage(message));
        return thunkAPI.rejectWithValue();
    }
});
export const deleteReservasByHorario = createAsyncThunk("reserva/deleteReservasByHorario", async ({ horarioId }, thunkAPI) => {
    try {
        const response = await reservaService.deleteReservasByHorario(horarioId);
        thunkAPI.dispatch(setMessage(response.data.message));
        return response.data;
    } catch (error) {
        const message =
            (error.response && error.response.data && error.response.data.message) ||
            error.message ||
            error.toString();
        thunkAPI.dispatch(setMessage(message));
        return thunkAPI.rejectWithValue();
    }
});
const initialState = {}
const reservaSlice = createSlice({
    name: "reserva",
    initialState,
    extraReducers: {
        [createReserva.fulfilled]: (state, action) => {
            state.isLoggedIn = true;
            state.user = action.payload.user;
        },
        [createReserva.rejected]: (state, action) => {
            state.isLoggedIn = true;
            state.user = action.payload.user;
        },
        [deleteReserva.fulfilled]: (state, action) => {
            state.isLoggedIn = true;
            state.user = action.payload.user;
        },
        [deleteReserva.rejected]: (state, action) => {
            state.isLoggedIn = true;
            state.user = action.payload.user;
        },
        [deleteReservasByHorario.fulfilled]: (state, action) => {
            state.isLoggedIn = true;
            state.user = action.payload.user;
        },
        [deleteReservasByHorario.rejected]: (state, action) => {
            state.isLoggedIn = true;
            state.user = action.payload.user;
        },
    },
});
const { reducer } = reservaSlice;
export default reducer;