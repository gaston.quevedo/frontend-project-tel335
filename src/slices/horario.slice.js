import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { setMessage } from "./message.slice";
import horarioService from "../services/horario.service";

export const decrementDisponibilidad = createAsyncThunk("horario/decrementDisponibilidad", async ({ id }, thunkAPI) => {
    try {
        const response = await horarioService.decrementDisponibilidad(id);
        thunkAPI.dispatch(setMessage(response.data.message));
        return response.data;
    } catch (error) {
        const message =
            (error.response && error.response.data && error.response.data.message) ||
            error.message ||
            error.toString();
        thunkAPI.dispatch(setMessage(message));
        return thunkAPI.rejectWithValue();
    }
});
export const incrementDisponibilidad = createAsyncThunk("horario/incrementDisponibilidad", async ({ id }, thunkAPI) => {
    try {
        const response = await horarioService.incrementDisponibilidad(id);
        thunkAPI.dispatch(setMessage(response.data.message));
        return response.data;
    } catch (error) {
        const message =
            (error.response && error.response.data && error.response.data.message) ||
            error.message ||
            error.toString();
        thunkAPI.dispatch(setMessage(message));
        return thunkAPI.rejectWithValue();
    }
});
export const getDisponibilidad = createAsyncThunk("horario/getDisponibilidad", async ({ id }, thunkAPI) => {
    try {
        const response = await horarioService.getDisponibilidad(id);
        thunkAPI.dispatch(setMessage(response.data.message));
        return response.data;
    } catch (error) {
        const message =
            (error.response && error.response.data && error.response.data.message) ||
            error.message ||
            error.toString();
        thunkAPI.dispatch(setMessage(message));
        return thunkAPI.rejectWithValue();
    }
});
export const setDisponibilidad = createAsyncThunk("horario/setDisponibilidad", async ({ id, disponibilidad }, thunkAPI) => {
    try {
        const response = await horarioService.setDisponibilidad(id, disponibilidad);
        thunkAPI.dispatch(setMessage(response.data.message));
        return response.data;
    } catch (error) {
        const message =
            (error.response && error.response.data && error.response.data.message) ||
            error.message ||
            error.toString();
        thunkAPI.dispatch(setMessage(message));
        return thunkAPI.rejectWithValue();
    }
});
const initialState = {}
const horarioSlice = createSlice({
    name: "horario",
    initialState,
    extraReducers: {
        [decrementDisponibilidad.fulfilled]: (state, action) => {
            state.isLoggedIn = true;
            state.user = action.payload.user;
        },
        [decrementDisponibilidad.rejected]: (state, action) => {
            state.isLoggedIn = true;
            state.user = action.payload.user;
        },
        [incrementDisponibilidad.fulfilled]: (state, action) => {
            state.isLoggedIn = true;
            state.user = action.payload.user;
        },
        [incrementDisponibilidad.rejected]: (state, action) => {
            state.isLoggedIn = true;
            state.user = action.payload.user;
        },
        [getDisponibilidad.fulfilled]: (state, action) => {
            state.isLoggedIn = true;
            state.user = action.payload.user;
        },
        [getDisponibilidad.rejected]: (state, action) => {
            state.isLoggedIn = true;
            state.user = action.payload.user;
        },
        [setDisponibilidad.fulfilled]: (state, action) => {
            state.isLoggedIn = true;
            state.user = action.payload.user;
        },
        [setDisponibilidad.rejected]: (state, action) => {
            state.isLoggedIn = true;
            state.user = action.payload.user;
        }
    },
});
const { reducer } = horarioSlice;
export default reducer;