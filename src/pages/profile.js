import React from "react";
import { useSelector } from "react-redux";
import Header from "../components/Header/Header.js";
import Footer from "../components/Footer/Footer.js";
import { Container, Col, Row } from "react-bootstrap";
import { Navigate } from 'react-router-dom';
import "../App.css";

const Profile = () => {
    const { user: currentUser } = useSelector((state) => state.auth);
    if (!currentUser)
        return <Navigate to="/" />
    return (
        <div>
            <Header />
            <section className="page-content">
                <Container align="left">
                    <Row>
                        <Col>
                            <h1><strong>Profile</strong></h1>
                            <br />
                            <p>
                                <strong>Name:</strong> {currentUser.name}
                            </p>
                            <p>
                                <strong>Email:</strong> {currentUser.email}
                            </p>
                            <p>
                                <strong>Token:</strong> {currentUser.accessToken.substring(0, 20)} ...{" "}
                                {currentUser.accessToken.substr(currentUser.accessToken.length - 20)}
                            </p>
                            <p>
                                <strong>Id:</strong> {currentUser.id}
                            </p>
                            <strong>Authorities:</strong>
                            <ul>
                                {currentUser.roles &&
                                currentUser.roles.map((role, index) => <li key={index}>{role}</li>)}
                            </ul>
                        </Col>
                    </Row>
                </Container>
            </section>
            <Footer />
        </div>
    )
}

export default Profile
