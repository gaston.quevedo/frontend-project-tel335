import Header from "../components/Header/Header.js";
import Footer from "../components/Footer/Footer.js";
import { Container} from 'react-bootstrap';
import "../App.css";

function About () {

    return (
        <div>
            <Header />
            <section className="page-content">
                <Container>
                    <h1>About page</h1>
                </Container>
            </section>
            <Footer />
        </div>
    )
}

export default About
