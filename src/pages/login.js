import Header from "../components/Header/Header.js";
import Footer from "../components/Footer/Footer.js";
import FormLogin from "../components/FormLogin/FormLogin.js";
import { Container } from 'react-bootstrap';
import "../App.css";

function Login () {

    return (
        <div>
            <Header />
            <section className="page-content">
                <Container className="col-lg-5" align="center">
                    <FormLogin />
                </Container>
            </section>
            <Footer />
        </div>
    )
}

export default Login
