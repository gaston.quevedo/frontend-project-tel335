import Header from "../components/Header/Header.js";
import Footer from "../components/Footer/Footer.js";
import FormSignUp from "../components/FormSignUp/FormSignUp.js";
import { Container } from 'react-bootstrap';
import "../App.css";

function SignUp () {

    return (
        <div>
            <Header />
            <section className="page-content">
                <Container className="col-lg-5" align="center">
                    <FormSignUp />
                </Container>
            </section>
            <Footer />
        </div>
    )
}

export default SignUp
