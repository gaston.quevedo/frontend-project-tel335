import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import UserService from "../services/user.service";
import eventBus from "../common/EventBus";
import Header from "../components/Header/Header";
import Footer from "../components/Footer/Footer";
import { Container} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import "../App.css";

const BoardAdmin = () => {
    const { user: currentUser } = useSelector((state) => state.auth);
    const [content, setContent] = useState("");
    useEffect(() => {
        UserService.getAdminBoard.then(
            (response) => {
                setContent(response.data);
            },
            (error) => {
                const _content =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();
                setContent(_content);
                if (error.response && error.response.status === 401) {
                    eventBus.dispatch("logout");
                }
            }
        );
    }, []);
    if (!currentUser)
        return <Navigate to="/" />
    return (
        <div>
            <Header />
            <section className="page-content">
                <Container>
                    <h3>{content}</h3>
                </Container>
            </section>
            <Footer />
        </div>
    );
};
export default BoardAdmin
