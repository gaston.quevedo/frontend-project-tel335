import React from "react";
import { useSelector } from "react-redux";
import Header from "../components/Header/Header";
import Footer from "../components/Footer/Footer";
import CalendarUI from "../components/Calendar/Calendar";
import { Container} from 'react-bootstrap';
import { Navigate } from "react-router-dom";
import "../App.css";

const Multipurpose = () => {
    const { user: currentUser } = useSelector((state) => state.auth)
    if (!currentUser)
        return <Navigate to="/" />
    return (
        <div>
            <Header />
            <section className="page-content">
                <Container align="center">
                    <CalendarUI />
                </Container>
            </section>
            <Footer />
        </div>
    );
};
export default Multipurpose