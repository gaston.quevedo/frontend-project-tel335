import Header from "../components/Header/Header.js";
import Footer from "../components/Footer/Footer.js";
import { Container, Button} from 'react-bootstrap';
import "../App.css";

function PageNotFound () {

    return (
        <div>
            <Header />
            <section className="page-content">
                <Container align="center">
                    <h1 className="error-404-text">404</h1>
                    <h1 className="page-not-found-text">Page Not Found</h1>
                    <p className="small-text">We were unable to find the page you were looking for.</p>
                    <Button id="light-blue-round-button" size="lg" href="/">Go to homepage</Button>
                </Container>
            </section>
            <Footer />
        </div>
    )
}

export default PageNotFound
