import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { Row, Col, Button } from 'react-bootstrap';
import { register } from "../../slices/auth.slice";
import { clearMessage } from "../../slices/message.slice";

const FormSignUp = () => {
    const [successful, setSuccessful] = useState(false);
    const { message } = useSelector((state) => state.message);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(clearMessage());
    }, [dispatch]);
    const initialValues = {
        name: "",
        email: "",
        password: "",
    };
    const validationSchema = Yup.object().shape({
        name: Yup.string()
        .test(
            "len",
            "You must enter a name.",
            (val) =>
            val &&
            val.toString().length >= 0 &&
            val.toString().length <= 20
        )
        .required("This field is required!"),
        email: Yup.string()
        .email("This is not a valid email.")
        .required("This field is required!"),
        password: Yup.string()
        .test(
            "len",
            "The password must be between 6 and 40 characters.",
            (val) =>
            val &&
            val.toString().length >= 6 &&
            val.toString().length <= 40
        )
        .required("This field is required!"),
    });
    const handleRegister = (formValue) => {
        const { name, email, password } = formValue;
        setSuccessful(false);
        dispatch(register({ name, email, password }))
        .unwrap()
        .then(() => {
            setSuccessful(true);
        })
        .catch(() => {
            setSuccessful(false);
        });
    };
    return (
        <div className="card card-container bg-white shadow-sm rounded p-5">
            <h1>Sign up</h1>
            <br />
            <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={handleRegister}>
                <Form align="left">
                {!successful ? (
                    <div>
                        <div className="form-group mb-3">
                            <label>Full Name</label>
                            <Field name="name" type="text" className="form-control" placeholder="Full Name" />
                            <ErrorMessage name="name" component="div" className="alert alert-danger" />
                        </div>
                        <div className="form-group mb-3">
                            <label>Email address</label>
                            <Field name="email" type="email" className="form-control" placeholder="Email" />
                            <ErrorMessage name="email" component="div" className="alert alert-danger" />
                        </div>
                        <div className="form-group mb-3">
                            <label>Password</label>
                            <Field name="password" type="password" className="form-control" placeholder="Password" />
                            <ErrorMessage name="password" component="div" className="alert alert-danger" />
                        </div>
                        <div className="form-group d-grid" align="center">
                            <Button id="light-blue-round-button" variant="primary" type="submit">
                                Sign up
                            </Button>
                        </div>
                        <Row>
                            <Col align="center">
                                <label>Already have an account? Click <a href="/login">here</a> to log in.</label>
                            </Col>
                        </Row>
                    </div>
                ): (
                    <div className="d-grid" align="center">
                        <Button id="light-blue-round-button" href="/login">
                            Continue
                        </Button>
                    </div>
                )}
                </Form>
            </Formik>
            {message && (
                <div className="form-group">
                    <div className={successful ? "alert alert-success" : "alert alert-danger"} role="alert">
                        {message}
                    </div>
                </div>
            )}
        </div>
    )
}
// Fix Already have an account? after successfull sign up. Fixed??
export default FormSignUp