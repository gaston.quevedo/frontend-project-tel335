import { Container, Row, Col} from "react-bootstrap";
import './Footer.css';

function Footer () {
    return (
        <section id='footer'>
            <Container align="center">
                <Row>
                    <Col>
                        <p>© 2022, Valentina Espinoza - Gastón Quevedo - Josué Sandoval. All rights reserved.</p>
                    </Col>
                </Row>
            </Container>
        </section>
    )
}

export default Footer