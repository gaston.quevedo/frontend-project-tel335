import React, { useState, useEffect  } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Navigate } from "react-router-dom";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { Row, Col, Button } from 'react-bootstrap';
import { login } from "../../slices/auth.slice";
import { clearMessage } from "../../slices/message.slice";

const FormLogin = (props) => {
    const [loading, setLoading] = useState(false);
    const { isLoggedIn } = useSelector((state) => state.auth);
    const { message } = useSelector((state) => state.message);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(clearMessage());
    }, [dispatch]);
    const initialValues = {
        email: "",
        password: ""
    };
    const validationSchema = Yup.object().shape({
        email: Yup.string().required("This field is required!"),
        password: Yup.string().required("This field is required!"),
    });
    const handleLogin = (formValue) => {
        const { email, password } = formValue;
        setLoading(true);
        dispatch(login({ email, password })).unwrap().then(() => {
            props.history.push("/profile");
            window.location.reload();
        })
        .catch(() => {
            setLoading(false);
        });
    };
    if (isLoggedIn) {
        return <Navigate to="/profile" />  //Instead of going to profile, perhaps just go to some sort of dashboard/home
    }
    return (
        <div className="card card-container bg-white shadow-sm rounded p-5">
            <h1>Log in</h1>
            <br />
            <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={handleLogin}>
                <Form align="left">
                    <div className="form-group mb-3">
                        <label htmlFor="email">Email address</label>
                        <Field name="email" type="email" placeholder="Email" className="form-control" />
                        <ErrorMessage name="email" component="div" className="alert alert-danger" />
                    </div>
                    <div className="form-group mb-3">
                        <label htmlFor="password">Password</label>
                        <Field name="password" type="password" placeholder="Password" className="form-control" />
                        <ErrorMessage name="password" component="div" className="alert alert-danger" />
                    </div>
                    <div className="form-group d-grid" align="center">
                        <Button id="light-blue-round-button" variant="primary" type="submit" disabled={loading}>
                            {loading && (
                                <span className="spinner-border spinner-border-sm"></span>
                            )}
                            <span>Login</span>
                        </Button>
                    </div>
                </Form>
            </Formik>
            {message && (
                <div className="form-group">
                    <div className="alert alert-danger" role="alert">
                        {message}
                    </div>
                </div>
            )}
            <Row>
                <Col>
                    <label>Don't have an account? <a href="/signup">Create an account</a></label>
                </Col>
            </Row>
        </div>
    )
}
export default FormLogin