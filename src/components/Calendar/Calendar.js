import React, { useState} from 'react';

import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';
import {Button, Modal, Container, Col, Row} from "react-bootstrap"
import HorarioService from "../../services/horario.service";

function CalendarUI() {
    const [date, setDate] = useState(new Date());
    const [showTime, setShowTime] = useState(false) 

   // Estado para el cuadro de dialogo
   const [show, setShow] = useState(false);
   const handleClose = () => setShow(false);
   const handleShow = () => setShow(true);

   //Dropdown
   const [value_bloque,setValue_bloque]=useState('');
    const handleSelect_bloque=(e)=>{
        console.log(e);
        setValue_bloque(e)
    }
    const [value_dia,setValue_dia]=useState('');
    const handleSelect_dia=(e)=>{
        console.log(e);
        setValue_dia(e)
    }

    const handleDisponibilidad = (value)=> {
        //const dia = value_dia;
        //const bloque = value_bloque;
        
        const {dia, bloque} =value
        HorarioService.decrementDisponibilidad({ dia, bloque })
        setShow(false)
    };

    return (
        <div className='app'>
            <div>
                <Calendar onChange={setDate} value={date} onClickDay={() => {setShowTime(true);
                handleShow()
                }}/>
                
            </div>
            <div><Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Reservación</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    <Container>
                        <Row>
                            <Col>                        
                            <DropdownButton
                            alignRight
                            title="Seleccione un bloque"
                            id="dropdown-menu-align-right"
                            onSelect={handleSelect_bloque}
                                >
                                    <Dropdown.Item eventKey="1-2">1-2</Dropdown.Item>
                                    <Dropdown.Item eventKey="3-4">3-4</Dropdown.Item>
                                    <Dropdown.Item eventKey="5-6">5-6</Dropdown.Item>
                                    <Dropdown.Item eventKey="7-8">7-8</Dropdown.Item>
                                    <Dropdown.Item eventKey="9-10">9-10</Dropdown.Item>
                                    <Dropdown.Item eventKey="11-12">11-12</Dropdown.Item>
                                    <Dropdown.Item eventKey="13-14">13-14</Dropdown.Item>
                                    <Dropdown.Item eventKey="15-16">15-16</Dropdown.Item>
                                    <Dropdown.Item eventKey="17-18">17-18</Dropdown.Item>
                            </DropdownButton>
                            <h7>Usted a seleccionado el bloque:  {value_bloque}</h7></Col>
                            <Col>
                            <DropdownButton
                            alignRight
                            title="Seleccione un dia"
                            id="dropdown-menu-align-right"
                            onSelect={handleSelect_dia}
                                >
                                    <Dropdown.Item eventKey="Lunes">Lunes</Dropdown.Item>
                                    <Dropdown.Item eventKey="Martes">Martes</Dropdown.Item>
                                    <Dropdown.Item eventKey="Miercoles">Miércoles</Dropdown.Item>
                                    <Dropdown.Item eventKey="Jueves">Jueves</Dropdown.Item>
                                    <Dropdown.Item eventKey="Viernes">Viernes</Dropdown.Item>
                                    <Dropdown.Item eventKey="Sabado">Sábado</Dropdown.Item>
                            </DropdownButton>
                            <h7>Usted a seleccionado el día:  {value_dia}</h7></Col>
                            
                        </Row>
                        </Container>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Declinar
                        </Button>
                        <Button variant="primary"  onClick={() => {
                            HorarioService.decrementDisponibilidad({value_dia , value_bloque});setShow(false);}
                            }>Confirmar</Button>
                    </Modal.Footer>
                </Modal></div>

        </div>
    )
}
export default CalendarUI
