import React, { useState, useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../../slices/auth.slice";
import { Container, Navbar, Nav, Button, NavDropdown} from "react-bootstrap";
import eventBus from '../../common/EventBus';
import './Header.css';

function Header () {
    const [showModeratorBoard, setShowModeratorBoard] = useState(false);
    const [showAdminBoard, setShowAdminBoard] = useState(false);
    const { user: currentUser } = useSelector((state) => state.auth);
    const dispatch = useDispatch();
    const logOut = useCallback(() => {
        dispatch(logout());
    }, [dispatch]);
    useEffect(() => {
        if (currentUser) {
            setShowModeratorBoard(currentUser.roles.includes("ROLE_MODERATOR"));
            setShowAdminBoard(currentUser.roles.includes("ROLE_ADMIN"));
        } else {
            setShowModeratorBoard(false);
            setShowAdminBoard(false);
        }
        eventBus.on("logout", () => {
            logOut();
        });
        return () => {
            eventBus.remove("logout");
        };
    }, [currentUser, logOut]);
    return (
        <section id='header'>
            <Navbar className="header" collapseOnSelect expand="lg" bg="shadow-sm" variant="light" sticky="top" align="left">
                <Container>
                    <Navbar.Brand href="/">
                        <img
                            alt=""
                            src="pictures/LogoApp.png"
                            width="40"
                            height="40"
                            className="d-inline-block align-top"
                            />{' '}
                        Prestigio.json
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link href="/">Home</Nav.Link>
                            <Nav.Link href="/about">About</Nav.Link>
                            {showModeratorBoard && (<Nav.Link href="/mod">Moderator Board</Nav.Link>)}
                            {showAdminBoard && (<Nav.Link href="/admin">Admin Board</Nav.Link>)}
                            {currentUser && (
                                <Nav.Link href="/user">User</Nav.Link>)&&(
                                <NavDropdown title="Gym 3">
                                    <NavDropdown.Item href="/bodybuilding">Sala De Musculación</NavDropdown.Item>
                                    <NavDropdown.Item href="/multipurpose">Sala Multiuso</NavDropdown.Item>
                                </NavDropdown>)
                            }
                        </Nav>
                        <Nav className="ml-auto">
                            {currentUser ? (
                                <div>
                                    <Nav.Link href="/profile">{currentUser.name}</Nav.Link>
                                    <Button id="buttonHeader" href="/login" onClick={logOut}>Logout</Button>
                                </div>
                            ) : (
                                <div>
                                    <Button id="buttonHeader" href="/signup">Sign up</Button>
                                </div>
                            )}
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </section>
    )
}

export default Header