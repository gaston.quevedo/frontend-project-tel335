import React from 'react';
import ReactDOM from 'react-dom/client';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Routes, Route} from 'react-router-dom';
import { Provider } from 'react-redux';
import './index.css';
import App from './App';
import About from './pages/about.js';
import SignUp from './pages/signup.js';
import Login from './pages/login.js';
import Profile from './pages/profile';
import Multipurpose from './pages/multipurpose';
import Bodybuilding from './pages/bodybuilding';
import BoardUser from './pages/boarduser.js';
import BoardModerator from './pages/boardmoderator.js';
import BoardAdmin from './pages/boardadmin.js';
import PageNotFound from './pages/pagenotfound.js';
import store from "./store";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <Provider store={store}>
      <Routes>
        <Route exact path='/' element={<App />}></Route>
        <Route exact path='/about' element={<About />}></Route>
        <Route exact path='/login' element={<Login/>}></Route>
        <Route exact path='/signup' element={<SignUp/>}></Route>
        <Route exact path='/profile' element={<Profile/>}></Route>
        <Route exact path='/multipurpose' element={<Multipurpose/>}></Route>
        <Route exact path='/bodybuilding' element={<Bodybuilding/>}></Route>
        <Route path='/user' element={<BoardUser/>}></Route>
        <Route path='/mod' element={<BoardModerator/>}></Route>
        <Route path='/admin' element={<BoardAdmin/>}></Route>
        <Route path='*' element={<PageNotFound/>}></Route>
      </Routes>
    </Provider>
  </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
