import { configureStore } from '@reduxjs/toolkit'
import authReducer from "./slices/auth.slice";
import messageReducer from "./slices/message.slice";
import horarioReducer from "./slices/horario.slice";
import reservaReducer from "./slices/reserva.slice";

const reducer = {
    auth: authReducer,
    message: messageReducer,
    horario: horarioReducer,
    reserva: reservaReducer
}
const store = configureStore({
    reducer: reducer,
    devTools: true,
})
export default store;
