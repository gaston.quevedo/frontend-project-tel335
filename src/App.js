import React, { useState, useEffect } from "react";
import { useSelector} from "react-redux";
import UserService from "./services/user.service";
import Header from "./components/Header/Header";
import Login from "./components/FormLogin/FormLogin";
import Footer from "./components/Footer/Footer.js";
import { Container, Col, Row } from "react-bootstrap";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  const [content, setContent] = useState("");
  //const [showModeratorBoard, setShowModeratorBoard] = useState(false);
  //const [showAdminBoard, setShowAdminBoard] = useState(false);
  const { user: currentUser } = useSelector((state) => state.auth);
  useEffect(() => {
    UserService.getPublicContent().then(
      (response) => {
        setContent(response.data);
      },
      (error) => {
        const _content =
          (error.response && error.response.data) ||
          error.message ||
          error.toString();
        setContent(_content);
      }
    );
  }, []);
  /*useEffect(() => {
    if (currentUser) {
        setShowModeratorBoard(currentUser.roles.includes("ROLE_MODERATOR"));
        setShowAdminBoard(currentUser.roles.includes("ROLE_ADMIN"));
    } else {
        setShowModeratorBoard(false);
        setShowAdminBoard(false);
    }
  }, [currentUser]);*/
  return (
    <div className="App">
      <Header />
      <section className="page-content">
          <Container align="center">
              <Row>
                  <Col sm={8} className="col1">
                    <h1><span className="build-text">BUILD</span><span className="your-body-text">YOUR BODY</span></h1>
                    <h1><span className="transform-text">TRANSFORM</span><span className="your-life-text">YOUR LIFE</span></h1>
                  </Col>
                  <Col sm={4} className="col2">
                      { currentUser ? (
                        <h3>{content}</h3>
                      ) : (
                        <Login />
                      )}
                  </Col>
              </Row>
          </Container>
      </section>
      <Footer />
    </div>
  );
}

export default App
