import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "http://localhost:8080/api/horario/";
const decrementDisponibilidad = (dia, bloque) => {
    return axios.put(API_URL + "disponibilidad/decrement", {
        dia,
        bloque
    });
};
const incrementDisponibilidad = (dia, bloque) => {
    return axios.put(API_URL + "disponibilidad/increment", {
        headers: authHeader(),
        dia,
        bloque
    });
};
const getDisponibilidad = (id) => {
    return axios.get(API_URL + "disponibilidad", {
        id
    });
};
const setDisponibilidad = (id, disponibilidad) => {
    return axios.put(API_URL + "disponibilidad", {
        headers: authHeader(),
        id,
        disponibilidad
    });
};
const getIdHorario = (dia, bloque) => {
    return axios.get(API_URL + "id", {
        dia,
        bloque
    });
};
const horarioService = {
    decrementDisponibilidad,
    incrementDisponibilidad,
    getDisponibilidad,
    setDisponibilidad,
    getIdHorario
};
export default horarioService;
