import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "http://localhost:8080/api/reserva/";
const createReserva = (horarioId, userId) => {
    return axios.post(API_URL + "create", {
        headers: authHeader(),
        horarioId,
        userId
    });
};
const deleteReserva = (horarioId, userId) => {
    return axios.delete(API_URL + "delete", {
        headers: authHeader(),
        horarioId,
        userId
    });
};
const deleteReservasByHorario = (horarioId) => {
    return axios.delete(API_URL + "deleteAllByBloque", {
        headers: authHeader(),
        horarioId
    });
};
const reservaService = {
    createReserva,
    deleteReserva,
    deleteReservasByHorario
};
export default reservaService;